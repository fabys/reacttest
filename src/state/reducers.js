import app from "../app/state/reducer/app";
import {user} from "../app/pages/login/state/reducer";

export default {
  app,
  user
};
