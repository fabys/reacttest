import {createStore, compose as origCompose, applyMiddleware, combineReducers} from "redux";
import {createBrowserHistory} from "history";
import {connectRouter, routerMiddleware} from "connected-react-router";
import thunk from "redux-thunk";
import reducers from "./reducers";

const compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || origCompose;

export const history = createBrowserHistory();

export const store = createStore(connectRouter(history)(combineReducers(reducers)), compose(applyMiddleware(routerMiddleware(history), thunk)));
