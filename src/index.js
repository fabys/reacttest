import React from "react";
import ReactDOM from "react-dom";
import App from "./app/app";
import registerServiceWorker from "./registerServiceWorker";
import {Provider} from "react-redux";
import {store, history} from "./state/store";
import {MuiThemeProvider, createMuiTheme} from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#60ad5e",
      main: "#2e7d32",
      dark: "#005005"
    },
    secondary: {
      light: "#fff350",
      main: "#ffc107",
      dark: "#c79100"
    }
  }
});

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <App history={history} />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
