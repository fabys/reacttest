import React, {Component} from "react";
import PropTypes from "prop-types";
import {ConnectedRouter} from "connected-react-router";
import {Route, Switch} from "react-router";

import Nav from "./views/nav/nav";

import NotFound from "./pages/notfound/notfound";
import Login, {loginSuccess} from "./pages/login/login";

import Start from "./pages/start/start";
import Account from "./pages/account/account";

import "@material/react-material-icon/dist/material-icon.css";
import "./app.css";

class App extends Component {
  constructor(props) {
    super(props);

    const jwt = localStorage.getItem("jwt");
    if (jwt) {
      try {
        loginSuccess(jwt);
      } catch (err) {
        console.error(err);
      }
    }
  }

  render() {
    return (
      <div className="app">
        <ConnectedRouter history={this.props.history}>
          <div className="main">
            <Nav history={this.props.history} />

            <div className="content">
              <Switch>
                <Route exact path="/" component={Start} />
                <Route path="/login" component={Login} />
                <Route path="/account" component={Account} />
                <Route component={NotFound} />
              </Switch>
            </div>
          </div>
        </ConnectedRouter>
      </div>
    );
  }
}

App.propTypes = {
  history: PropTypes.object
};

export default App;
