export async function resolveError(res) {
  let err;
  if (res.json) {
    try {
      err = await res.json();
    } catch (_) {
      err = res;
    }
  } else {
    err = res;
  }

  let error;
  if (err.error) {
    error = err.error;
  } else {
    error = "An unknown error occured";
  }

  console.error(err, error);

  return error;
}
