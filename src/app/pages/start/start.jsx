import React, {Component} from "react";
import {NavLink} from "react-router-dom";

import LogoBox from "../../components/logobox/logobox";
import "./start.css";

class Start extends Component {
  render() {
    return (
      <div className="start">
        <div className="head-container">
          <LogoBox />
        </div>

        <div className="slogan">
          <h2>Monitor web pages and api endpoints of your projects. </h2>
        </div>

        <div className="info">
          <div>
            Created by Patrick Malleier & Fabian Pfeiffer |
            <NavLink to="/info" title="Info">
              Info
            </NavLink>
          </div>
        </div>
      </div>
    );
  }
}

export default Start;
