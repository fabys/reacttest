import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import LogoBox from "../../components/logobox/logobox";

import "./account.css";

class Account extends Component {
  render() {
    if (!this.props.user.data) {
      return null;
    }

    return (
      <div className="account">
        <div className="head-container">
          <LogoBox />
        </div>
        <div className="account-container">
          <div className="account-box">
            <ul>
              <li>
                <span className="label">UUID:</span>
                <span> {this.props.user.data.uuid}</span>
              </li>
              <li>
                <span className="label">E-Mail:</span>
                <span> {this.props.user.data.email}</span>
              </li>
              <li>
                <span className="label">Surname:</span>
                <span> {this.props.user.data.sur_name}</span>
              </li>
              <li>
                <span className="label">Given name:</span>
                <span> {this.props.user.data.given_name}</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    if (!this.props.user.data) {
      this.props.history.push("/login");
    }
  }
}

Account.propTypes = {
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(Account);
