import React, {Component} from "react";
import MaterialIcon from "@material/react-material-icon";

import "./notfound.css";

class NotFound extends Component {
  render() {
    return (
      <div className="notfound">
        <MaterialIcon icon="explore_off" className="icon" />
        <h1>This page does not exist!</h1>
        <h2>Error 404</h2>
      </div>
    );
  }
}

export default NotFound;
