import React, {Component} from "react";
import Button from "@material-ui/core/Button";
import {withStyles} from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import TextField from "@material-ui/core/TextField";

import LogoBox from "../../components/logobox/logobox";
import Loading from "../../components/loading/loading";

import config from "../../config";

import "./login.css";
import {resolveError} from "../../utils/utils";
import {store} from "../../../state/store";
import {login} from "./state/actions";

const styles = (theme) => ({
  button: {
    margin: theme.spacing.unit
  }
});

export const loginSuccess = (jwt) => {
  const jwtParts = jwt.split(".");
  let userData;
  try {
    userData = JSON.parse(atob(jwtParts[1]));
  } catch (e) {
    console.error(e);
    throw jwt;
  }

  if (!userData.user) {
    console.error("Invalid user", userData);
    throw jwt;
  }

  localStorage.setItem("jwt", jwt);
  store.dispatch(login(jwt, userData.user));
};

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      registrationSuccessfull: false,
      openErrorDialog: false,
      errorText: "",

      login_email: "",
      login_password: "",

      register_email: "",
      register_surname: "",
      register_givenname: ""
    };

    this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
    this.handleRegisterSubmit = this.handleRegisterSubmit.bind(this);
  }

  render() {
    return (
      <div className="login">
        <LogoBox />
        <div className="login-box">
          <Loading size="100px" inprogress={this.state.loading} />

          <div className="loginform-box box">
            <section id="login">
              <h1>Login</h1>

              <form onSubmit={this.handleLoginSubmit} autoComplete="on">
                <TextField required type="email" label="E-Mail" value={this.state.login_email} onChange={this.handleInputChange("login_email")} margin="normal" />

                <TextField required type="password" label="Password" value={this.state.login_password} onChange={this.handleInputChange("login_password")} margin="normal" />

                <Button type="submit" variant="contained" color="primary" className={this.props.classes.button}>
                  Login
                </Button>
              </form>
            </section>
          </div>
          <div className="separator">&nbsp;</div>
          <div className="register-box box">
            <section id="register">
              <h1>Register</h1>
              <p>If you do not have an account yet, create one for free.</p>

              <form onSubmit={this.handleRegisterSubmit} autoComplete="on">
                <TextField required type="email" label="E-Mail" value={this.state.register_email} onChange={this.handleInputChange("register_email")} margin="normal" />

                <TextField required label="Surname" value={this.state.register_surname} onChange={this.handleInputChange("register_surname")} margin="normal" />

                <TextField required label="Given name" value={this.state.register_givenname} onChange={this.handleInputChange("register_givenname")} margin="normal" />

                <Button type="submit" variant="outlined" color="primary" className={this.props.classes.button}>
                  Register
                </Button>
              </form>
            </section>
          </div>
        </div>

        <Dialog open={this.state.registrationSuccessfull}>
          <DialogTitle>Registration successfull</DialogTitle>
          <DialogContent>
            <DialogContentText>Check your emails for your password.</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button color="primary" onClick={this.handleRegistrationSuccessfullClose.bind(this)}>
              OK
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog open={this.state.openErrorDialog}>
          <DialogTitle>Error</DialogTitle>
          <DialogContent>
            <DialogContentText style={{color: "red"}}>{this.state.errorText}</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button color="primary" onClick={this.handleErrorDialogClose.bind(this)}>
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }

  componentDidMount() {
    const state = store.getState();

    if (state.user.jwt) {
      this.props.history.push("/");
    }
  }

  handleInputChange = (name) => (event) => {
    this.setState({
      [name]: event.target.value
    });
  };

  async handleLoginSubmit(e) {
    e.preventDefault();

    this.setState({loading: true});

    try {
      const res = await fetch(config.API_URL + "/login", {
        method: "POST",
        body: JSON.stringify({mail: this.state.login_email, pass: this.state.login_password})
      });

      if (!res.ok) {
        throw res;
      }

      const json = await res.json();
      if (json.error) {
        throw json;
      }

      if (!json.jwt) {
        throw json;
      }

      loginSuccess(json.jwt);

      this.setState({loading: false, login_email: "", login_password: ""});
      this.props.history.push("/");
    } catch (e) {
      this.setState({errorText: await resolveError(e), openErrorDialog: true});
    }
  }

  async handleRegisterSubmit(e) {
    e.preventDefault();

    this.setState({loading: true});

    try {
      const res = await fetch(config.API_URL + "/register", {
        method: "POST",
        body: JSON.stringify({mail: this.state.register_email, sur_name: this.state.register_surname, given_name: this.state.register_givenname})
      });

      if (!res.ok) {
        throw res;
      }

      this.setState({registrationSuccessfull: true, loading: false, register_email: "", register_surname: "", register_givenname: ""});
    } catch (e) {
      this.setState({errorText: await resolveError(e), openErrorDialog: true});
    }
  }

  handleRegistrationSuccessfullClose() {
    this.setState({registrationSuccessfull: false});
  }

  handleErrorDialogClose() {
    this.setState({openErrorDialog: false, loading: false});
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default withStyles(styles)(Login);
