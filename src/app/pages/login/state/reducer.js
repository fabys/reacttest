import {loginActions} from "./actions";

const defaultState = {};

export function user(state = defaultState, action) {
  switch (action.type) {
    case loginActions.LOGIN:
      return {
        ...state,
        jwt: action.payload.jwt,
        data: action.payload.data
      };
    case loginActions.LOGOUT:
      return defaultState;
    default:
      return state;
  }
}
