export const loginActions = {
  LOGIN: "LOGIN_LOGIN",
  LOGOUT: "LOGIN_LOGOUT"
};

export const login = (jwt, data) => {
  const payload = {
    jwt,
    data
  };

  return {
    type: loginActions.LOGIN,
    payload
  };
};

export const logout = () => {
  return {
    type: loginActions.LOGOUT
  };
};
