import React, {Component} from "react";

import Logo from "../../../assets/logo.svg";

import "./logobox.css";

class LogoBox extends Component {
  render() {
    return (
      <div className="logobox">
        <img className="logo" src={Logo} alt="MetricX" />
        <h1>MetricX</h1>
      </div>
    );
  }
}

export default LogoBox;
