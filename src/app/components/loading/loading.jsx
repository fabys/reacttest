import React, {Component} from "react";
import PropTypes from "prop-types";

import "./loading.css";

class Loading extends Component {
  render() {
    return (
      <div className="loading-box" style={{display: !this.props.inprogress ? "none" : ""}}>
        <div className="loading-container">
          <div className="backdrop" style={{display: !this.props.backdrop ? "none" : ""}} />
          <div className="loading" style={{width: this.props.size, height: this.props.size, "--loading-color": this.props.color}}>
            <div />
            <div />
          </div>
        </div>
      </div>
    );
  }
}

Loading.defaultProps = {
  size: "200px",
  backdrop: true,
  color: "var(--mdc-theme-primary)",
  inprogress: false
};

Loading.propTypes = {
  size: PropTypes.string,
  backdrop: PropTypes.bool,
  color: PropTypes.string,
  inprogress: PropTypes.bool
};

export default Loading;
