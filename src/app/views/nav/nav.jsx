import React, {Component} from "react";
import MaterialIcon from "@material/react-material-icon";
import {NavLink} from "react-router-dom";
import Ripple from "react-material-ripple";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Logo from "../../../assets/logo.svg";
import "./nav.css";
import {store} from "../../../state/store";
import {logout} from "../../pages/login/state/actions";

class Nav extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.activeLinkClass = this.activeLinkClass.bind(this);

    this.props.history.listen(() => {
      this.forceUpdate();
    });
  }

  render() {
    const loginoutLink = this.props.user.jwt ? (
      <li>
        <a href="" title="Logout" onClick={this.handleLogout}>
          <Ripple>
            <MaterialIcon icon="power_settings_new" className="icon" />
          </Ripple>
        </a>
      </li>
    ) : (
      <li>
        <NavLink to="/login" title="Login / Register" activeClassName="" className={this.activeLinkClass("/login")}>
          <Ripple>
            <MaterialIcon icon="input" className="icon" />
          </Ripple>
        </NavLink>
      </li>
    );

    return (
      <nav className="nav">
        <ul>
          <li>
            <NavLink to="/" title="MetricX">
              <Ripple>
                <img className="icon" src={Logo} alt="logo" />
              </Ripple>
            </NavLink>
          </li>
          <li className="separator" />
          {loginoutLink}
          <li>
            <NavLink to="/account" disabled={this.props.user.jwt === undefined} title="Account" activeClassName="" className={this.activeLinkClass("/account")} onClick={this.handleClick}>
              <Ripple>
                <MaterialIcon icon="perm_identity" className="icon" />
              </Ripple>
            </NavLink>
          </li>
          <li className="separator" />
          <li>
            <NavLink to="/dashboard" disabled title="Dashboard" activeClassName="" className={this.activeLinkClass("/dashboard")} onClick={this.handleClick}>
              <Ripple>
                <MaterialIcon icon="dashboard" className="icon" />
              </Ripple>
            </NavLink>
          </li>
          <li>
            <NavLink to="/sensors" disabled title="Sensors" activeClassName="" className={this.activeLinkClass("/sensors")} onClick={this.handleClick}>
              <Ripple>
                <MaterialIcon icon="view_comfy" className="icon" />
              </Ripple>
            </NavLink>
          </li>
          <li>
            <NavLink to="/alarms" disabled title="Alarms" activeClassName="" className={this.activeLinkClass("/alarms")} onClick={this.handleClick}>
              <Ripple>
                <MaterialIcon icon="notification_important" className="icon" />
              </Ripple>
            </NavLink>
          </li>
          <li className="separator" />
          <li>
            <NavLink to="/info" disabled title="Info" activeClassName="" className={this.activeLinkClass("/info")} onClick={this.handleClick}>
              <Ripple>
                <MaterialIcon icon="info" className="icon" />
              </Ripple>
            </NavLink>
          </li>
        </ul>
      </nav>
    );
  }

  handleClick(e) {
    if (e.currentTarget.hasAttribute("disabled")) {
      e.preventDefault();
    }
  }

  handleLogout(e) {
    e.preventDefault();

    store.dispatch(logout());
    localStorage.removeItem("jwt");
    this.props.history.push("/");
  }

  activeLinkClass(path) {
    return window.location.pathname === path ? "active" : "";
  }
}

Nav.propTypes = {
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(Nav);
