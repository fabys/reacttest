FROM node:10.8-alpine

WORKDIR /var/www

COPY _server/package.json _server/server.js _server/yarn.lock ./
COPY build ./build

RUN yarn install --non-interactive --frozen-lockfile

EXPOSE 8080

CMD [ "yarn", "start" ]